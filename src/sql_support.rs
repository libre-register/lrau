//! # Diesel Support
//!
//! Diesel support is included in this module. Moreover, to activate it, use
//! the embeded migrations (or write your own.) The schema is included from lib.

use super::User;
#[cfg(feature = "diesel-support")]
use crate::schema::*;
use crate::Permissions;
#[cfg(feature = "diesel-support")]
use ::diesel_migrations::embed_migrations;
#[cfg(feature = "sqlx_support")]
use sqlx::migrate::Migrator;

#[cfg(feature = "diesel-support")]
embed_migrations!("diesel-migrations/");
#[cfg(feature = "diesel-support")]
pub mod diesel_migrations {
    pub use super::embedded_migrations::*;
}

/// This is the diesel version of a user, meaning it is not designed to be
/// worked with but rathre used as an intermediary between the databases
/// and the program.
#[derive(Clone, Debug, Hash, PartialEq, Eq, Ord, PartialOrd)]
#[cfg_attr(
    feature = "diesel-support",
    derive(Queryable, Insertable, Identifiable),
    primary_key(username),
    table_name = "login_users"
)]
pub struct SqlUser {
    /// This is then username / id of the user
    pub username: String,
    /// This is the password of the user.
    ///
    /// This is stores hashed with Argon2id.
    pub password: String,
    pub permissions: Permissions,
}

impl From<User> for SqlUser {
    fn from(them: User) -> Self {
        Self {
            username: them.username,
            password: them.password,
            permissions: them.permissions,
        }
    }
}

impl From<SqlUser> for User {
    fn from(them: SqlUser) -> Self {
        Self {
            username: them.username,
            password: them.password,
            permissions: them.permissions,
            logged_in: None,
            expire: None,
        }
    }
}

#[cfg(feature = "sqlx_support")]
pub static SQLX_MIGRATIONS: Migrator = {
    let mut migrator = sqlx::migrate!("./sqlx-migrations");
    migrator.ignore_missing = true;
    migrator
};
