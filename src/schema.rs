table! {
    login_users (username) {
        username -> Text,
        password -> Text,
        permissions -> Jsonb,
    }
}
