/// Here are some tests to test serde support.
///
/// Isn't serde just sweet 🍬!
#[test]
fn serde_test() -> argon2::Result<()> {
    // Get the permissions
    let permissions: lrau::Permissions =
        toml::from_str(include_str!("./generic.toml")).unwrap();

    // Create a user.
    let user = lrau::User::new(String::from("john_t"), "1234", permissions)?;

    // Serialise the user
    let serialised_user = toml::to_string(&user).unwrap();

    // Deserialise it
    assert_eq!(user, toml::from_str(&serialised_user).unwrap());

    Ok(())
}
