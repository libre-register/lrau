// Just a pretty generic test, nothing much to say.
#[test]
fn generic() -> argon2::Result<()> {
    // Serialisation
    //
    // If this unwrap fails, which it may do, than that is
    // problem number 1 with the test
    let permissions: lrau::Permissions =
        toml::from_str(include_str!("./generic.toml")).unwrap();

    // Test creating a user, a user who has an exeptionally
    // bad password!
    let mut user = lrau::User::new(
        String::from("john_t"),
        "1234",
        permissions,
    )?;

    println!("{:#?}", user);

    // Testing password authentication

    // Valid
    assert!(user.validate("1234")?);
    // Invalid
    assert!(!user.validate("12345")?);

    // Permissions

    // Root Permissions
    assert!(!user.get_permission::<&str>(&[], false));
    assert!(!user.get_permission::<&str>(&[], true));

    // Existing paths

    // No access
    assert!(!user.get_permission(&["contacts"], false));
    assert!(!user.get_permission(&["contacts"], true));
    assert!(!user.get_permission(&["server"], false));
    assert!(!user.get_permission(&["server"], true));
    assert!(!user.get_permission(&["server", "shutdown"], false));
    assert!(!user.get_permission(&["server", "shutdown"], true));

    // No Mut Access
    assert!(user.get_permission(&["contacts", "name"], false));
    assert!(!user.get_permission(&["contacts", "name"], true));

    // Mut Access
    assert!(user.get_permission(&["admin", "passwords"], true));

    // Nonexisting paths
    assert!(user.get_permission(&["admin", "passwords", "reset"], true));
    assert!(!user.get_permission(&["notathing"], false));

    // Checks if we have logged in (we haven't)
    assert!(!user.check_login());
    assert!(!user.check_valid_login());

    // User Login
    user.log_in("1234", std::time::Duration::from_millis(10))?;

    // Checks for logins
    assert!(user.check_login());
    assert!(user.check_valid_login());

    // Timeouts
    std::thread::sleep(std::time::Duration::from_millis(10));

    assert!(user.check_login());

    assert!(!user.check_valid_login());

    assert_eq!(
        user.get_valid_permissions(&["admin", "passwords", "reset"], true),
        Err(lrau::user::SessionExpired {}),
    );

    Ok(())
}
