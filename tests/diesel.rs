use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use lrau::schema;
use std::env;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url =
        env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

/// Here are some tests to test diesel support.
#[test]
fn diesel_test() -> Result<(), Box<dyn std::error::Error>> {
    use lrau::schema::login_users;
    use lrau::schema::login_users::dsl as users_dsl;
    use lrau::sql_support::SqlUser;

    // Get the permissions
    let permissions: lrau::Permissions =
        toml::from_str(include_str!("./generic.toml")).unwrap();

    // Create a user.
    let user = lrau::User::new(String::from("john_t"), "1234", permissions)?;

    // Get a raw user.
    let raw_user = SqlUser::from(user.clone());

    // Check it equals the non-raw user
    assert_eq!(user, raw_user.clone().into());

    // Grab a connection
    let conn = establish_connection();

    // Deletes any previous users.
    diesel::delete(
        login_users::table.filter(users_dsl::username.like("john_t")),
    )
    .execute(&conn)?;

    // Insert it
    let inserted_user: SqlUser =
        diesel::insert_into(schema::login_users::table)
            .values(&raw_user)
            .get_result(&conn)?;

    let inserted_user: lrau::User = inserted_user.into();

    assert_eq!(inserted_user, user);

    Ok(())
}
