CREATE TABLE LOGIN_USERS (
    -- The user's username
    username TEXT PRIMARY KEY,
    
    -- Hashes password.
    --
    -- Don't you dare store unhashed passwords you monster.
    password TEXT NOT NULL,
    
    -- The users permissions
    permissions JSONB NOT NULL
);
