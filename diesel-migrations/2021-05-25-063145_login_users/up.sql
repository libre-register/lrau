-- Your SQL goes here
CREATE TABLE LOGIN_USERS (
    -- The user's username
    username TEXT PRIMARY KEY,
    
    -- Hashed password.
    password TEXT NOT NULL,
    
    -- The users permissions
    permissions JSONB NOT NULL
);
