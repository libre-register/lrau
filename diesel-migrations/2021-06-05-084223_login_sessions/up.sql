CREATE TABLE login_sessions (
    id UUID PRIMARY KEY,
    username TEXT NOT NULL,
    expire DATETIME NOT NULL,
);
